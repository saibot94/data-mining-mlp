## Data Mining - Multilayer perceptron implementation

### Introduction

This is the project implementing the multilayer perceptron algorithm for data mining, spring semester 2017, AIDC I, West University of Timisoara.

There will be multiple versions of the algorithm, each working better for some specific goal and with different speeds. 

The python version is the simple one, whilst there'll also be a Scala implementation using Apache Spark, which will allow big data applications to use the Multilayer perceptron implementation.

### Running

In order to run the Python version, you just write some code that connects to the pymlp package. The dependencies for the python version can be found in the pymlp file: `requirements.txt`


